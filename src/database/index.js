const Sequelize = require('sequelize');
const dbConfig = require('../config/database');

const Admin = require('../models/Admin');
const User = require('../models/User');
const Film = require('../models/Film');
const FilmHasUsers = require('../models/FilmHasUsers');


const connection = new Sequelize(dbConfig);
try {
  connection.authenticate();
  console.log('Connection has been established successfully.');
} catch (error) {
  console.error('Unable to connect to the database:', error);
}

Admin.init(connection);
User.init(connection);
Film.init(connection);
FilmHasUsers.init(connection);

User.associate(connection.models);
Film.associate(connection.models);
FilmHasUsers.associate(connection.models);

module.exports = connection;