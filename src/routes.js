const express = require('express');
const authMiddleware = require('./middleware/authorization');

const AdminController = require('./controllers/AdminController');
const UserController = require('./controllers/UserController');
const FilmController = require('./controllers/FilmController');

const public = express.Router();
const private = express.Router();
private.use(authMiddleware);


public.post('/admins/create', AdminController.create);
private.get('/admins/read', AdminController.read);
private.put('/admins/update', AdminController.update);
private.delete('/admins/delete', AdminController.delete);
private.get('/admins/show', AdminController.show);
public.get('/admins/login', AdminController.login);

public.post('/users/create', UserController.create);
private.get('/users/read', UserController.read);
private.put('/users/update', UserController.update);
private.delete('/users/delete', UserController.delete);
private.get('/users/show', UserController.show);
public.get('/users/login', UserController.login);

private.post('/films/create', FilmController.create);
public.get('/films/read', FilmController.read);
private.put('/films/update', FilmController.update);
private.delete('/films/delete', FilmController.delete);
public.get('/films/show', FilmController.show);
private.get('/films/vote', FilmController.vote);

module.exports = [public, private];
