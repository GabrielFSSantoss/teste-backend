const { Model, DataTypes } = require('sequelize');

class FilmHasUsers extends Model {
  static init(sequelize) {
    super.init({
      vote: {
        type: DataTypes.INTEGER,
        allowNull: false,
      }
    },
      {
        sequelize,
        tableName: 'film_has_user',
        timestamps: false
      });
  }
  static associate(models) {

  }
}

module.exports = FilmHasUsers;