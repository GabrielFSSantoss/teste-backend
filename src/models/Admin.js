const { Model, DataTypes } = require('sequelize');

class Admin extends Model {
  static init(sequelize) {
    super.init({
      id_admin: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        lowercase: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      excluded: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
      {
        sequelize,
        tableName: 'admin',
        timestamps: false
      });
  }
}

module.exports = Admin;