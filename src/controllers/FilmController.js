const Admin = require('../models/Admin');
const User = require('../models/User');
const Film = require('../models/Film');
const uuid = require('uuid');
const Sequelize = require('sequelize');

module.exports = {
  async create(req, res) {
    try {

      const admin = await Admin.findByPk(req.entity.id);

      if (!admin) {
        return res.status(400).json({ error: 'Administrador not found' });
      }

      req.body.id_film = uuid.v4();
      const film = await Film.create(req.body);

      return res.json(film);

    } catch (error) {
      return res.status(400).json({ error: 'Film Exists' });
    }
  },

  async read(req, res) {
    try {

      let films

      if(req.body.title){
        films = await Film.findAll({ where:{title: req.body.title } });
        return res.json(films);
      }
      else if(req.body.director) {
        films = await Film.findAll({ where:{director: req.body.director } });
        return res.json(films);
      }
      else if(req.body.genres) {
        films = await Film.findAll({ where: { genres: { [Sequelize.Op.substring]: req.body.genres }}});
        return res.json(films);
      }
      else if(req.body.actors) {
        films = await Film.findAll({ where: { actors: { [Sequelize.Op.substring]: req.body.actors }}});
        return res.json(films);
      }
      else {
        const films = await Film.findAll();
        return res.json(films);
      }

    } catch (error) {
      return res.status(400).json({ error: 'Could not list' });
    }
  },

  async update(req, res) {
    try {

      const admin = await Admin.findByPk(req.entity.id);

      if (!admin) {
        return res.status(400).json({ error: 'Administrador not found' });
      }

      const film = await Film.findByPk(req.body.id_film);

      if (!film) {
        return res.status(400).json({ error: 'Film not found' });
      }

      delete req.body.id_film;
      await film.update(req.body);

      return res.json(film);

    } catch (error) {
      return res.status(400).json({ error: 'Could not update' });
    }
  },

  async delete(req, res) {
    try {

      const admin = await Admin.findByPk(req.entity.id);

      if (!admin) {
        return res.status(400).json({ error: 'Administrador not found' });
      }

      const film = await Film.findByPk(req.body.id_film);

      if (!film) {
        return res.status(400).json({ error: 'Film not found' });
      }

      await film.destroy(req.body);

      return res.send({ answer: "successfully deleted" });

    } catch (error) {
      return res.status(400).json({ error: 't was not possible to delete' });
    }
  },

  async show(req, res) {
    try {

      const film = await Film.findByPk(req.body.id_film);

      if (!film) {
        return res.status(400).json({ error: 'Film not found' });
      }

      return res.json(film);

    } catch (error) {
      return res.status(400).json({ error: 'Couldnt show' });
    }
  },

  async vote(req, res) {
    try {

      const user = await User.findByPk(req.entity.id);

      if (!user) {
        return res.status(400).json({ error: 'User not found' });
      }

      let film = await Film.findByPk(req.body.id_film);

      if (!film) {
        return res.status(400).json({ error: 'Film not found' });
      }

      await film.addUser(user, { through: { vote: req.body.vote } });

      film = await Film.findByPk(req.body.id_film, {
        include: [{ association: 'user' }],
      })

      let med = 0;
      let cont = 0;
    
      await film.user.map(user => {
        med = med + user.FilmHasUsers.vote;
        cont++;
      });
    
      med = (med/cont);

      await film.update({average: med});

      return res.send({ answer: "successfully voted" });

    } catch (error) {
      return res.status(400).json({ error: 'Couldnt vote' });
    }
  },
}