const { Model, DataTypes } = require('sequelize');

class User extends Model {
  static init(sequelize) {
    super.init({
      id_user: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        lowercase: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      excluded: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
      {
        sequelize,
        tableName: 'user',
        timestamps: false
      });
  }
  static associate(models) {
    this.belongsToMany(models.Film, { foreignKey: 'id_user', through: 'FilmHasUsers', as: 'film', timestamps: false});
  }
}

module.exports = User;