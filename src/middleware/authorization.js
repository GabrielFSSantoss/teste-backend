const jwt = require('jsonwebtoken');
const authConfig = require('../config/authSeed.json');

module.exports = (req, res, next) => {

  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(401).send({ error: "No token provided" });
  }

  const parts = authHeader.split(' ');

  if (!parts.length === 2) {
    return res.status(401).send({ error: "Token error" });
  }

  const [scheme, token] = parts;

  if (!/^Bearer$/i.test(scheme)) {
    return res.status(401).send({ error: "Token malformatted" });
  }

  try {
    const decoded = jwt.verify(token, authConfig.secret);
    const { id } = decoded;
    req.entity = { id }
    return next();
    
  } catch {
    return res.status(400).json({ error: 'Token ivalid.' });
  }

}