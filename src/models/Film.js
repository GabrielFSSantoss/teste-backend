const { Model, DataTypes } = require('sequelize');

class Film extends Model {
  static init(sequelize) {
    super.init({
      id_film: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      storyline: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      genres: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      director: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      actors: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      average: {
        type: DataTypes.BOOLEAN,
        defaultValue: 0,
      },
    },
      {
        sequelize,
        tableName: 'film',
        timestamps: false
      });
  }
   static associate(models) {
    this.belongsToMany(models.User, { foreignKey: 'id_film', through: 'FilmHasUsers', as: 'user', timestamps: false});
  }
}

module.exports = Film;