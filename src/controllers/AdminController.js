const Admin = require('../models/Admin');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const uuid = require('uuid')
const authConfig = require('../config/authSeed.json');

module.exports = {
  async create(req, res) {
    try {
      const hash = await bcrypt.hash(req.body.password, 10);

      req.body.password = hash;
      req.body.id_admin = uuid.v4();
      const admin = await Admin.create(req.body);

      return res.json(admin);

    } catch (error) {
      return res.status(400).json({ error: 'Admin Exists' });
    }
  },

  async read(req, res) {
    try {

      const admin = await Admin.findByPk(req.entity.id);

      if (!admin) {
        return res.status(400).json({ error: 'Administrador not found' });
      }

      const administrators = await Admin.findAll({ where: { excluded: false } });

      return res.json(administrators);

    } catch (error) {
      return res.status(400).json({ error: 'Could not list' });
    }
  },

  async update(req, res) {
    try {

      const admin = await Admin.findByPk(req.entity.id);

      if (!admin) {
        return res.status(400).json({ error: 'Administrador not found' });
      }

      if (req.body.password != req.body.repeatPassword) {
        return res.status(400).json({ error: 'Different passwords' });
      }

      if (req.body.password) {
        const hash = await bcrypt.hash(req.body.password, 10);
        req.body.password = hash;
      }

      await admin.update(req.body);

      return res.json(admin);

    } catch (error) {
      return res.status(400).json({ error: 'Could not update' });
    }
  },

  async delete(req, res) {

    try {

      const admin = await Admin.findByPk(req.entity.id);

      if (!admin) {
        return res.status(400).json({ error: 'Administrador not found' });
      }

      await admin.update({excluded: true});

      return res.send({ answer: "successfully deleted" });

    } catch (error) {
      return res.status(400).json({ error: 't was not possible to delete' });
    }
  },

  async show(req, res) {
    try {

      const admin = await Admin.findByPk(req.entity.id);

      if (!admin) {
        return res.status(400).json({ error: 'Administrador not found' });
      }

      return res.json(admin);

    } catch (error) {
      return res.status(400).json({ error: 'Couldnt show' });
    }
  },

  async login(req, res) {
    try {

      const admin = await Admin.findOne({ where: { email: req.headers.email } });

      if (!admin || admin.excluded) {
        return res.status(400).json({ error: 'Email not found' });
      }

      if (!req.headers.password || !await bcrypt.compare(req.headers.password, admin.password)) {
        return res.status(400).json({ error: 'Password invalid' });
      }

      const token = jwt.sign({ id: admin.id_admin }, authConfig.secret, {
        expiresIn: 86400,
      });

      return res.send({ admin, token });

    } catch (error) {
      return res.status(400).json({ error: 'Couldnt login' });
    }
  },
}