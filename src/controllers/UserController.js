const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const uuid = require('uuid')
const authConfig = require('../config/authSeed.json');

module.exports = {
  async create(req, res) {
    try {
      const hash = await bcrypt.hash(req.body.password, 10);

      req.body.password = hash;
      req.body.id_user = uuid.v4();
      const user = await User.create(req.body);

      return res.json(user);

    } catch (error) {
      return res.status(400).json({ error: 'User Exists' });
    }
  },

  async read(req, res) {
    try {

      const user = await User.findByPk(req.entity.id);

      if (!user) {
        return res.status(400).json({ error: 'User not found' });
      }

      const users = await User.findAll({ where: { excluded: false } });

      return res.json(users);

    } catch (error) {
      return res.status(400).json({ error: 'Could not list' });
    }
  },

  async update(req, res) {
    try {

      const user = await User.findByPk(req.entity.id);

      if (!user) {
        return res.status(400).json({ error: 'User not found' });
      }

      if (req.body.password != req.body.repeatPassword) {
        return res.status(400).json({ error: 'Different passwords' });
      }

      if (req.body.password) {
        const hash = await bcrypt.hash(req.body.password, 10);
        req.body.password = hash;
      }

      await user.update(req.body);

      return res.json(user);

    } catch (error) {
      return res.status(400).json({ error: 'Could not update' });
    }
  },

  async delete(req, res) {

    try {

      const user = await User.findByPk(req.entity.id);

      if (!user) {
        return res.status(400).json({ error: 'User not found' });
      }

      await user.update({excluded: true});

      return res.send({ answer: "successfully deleted" });

    } catch (error) {
      return res.status(400).json({ error: 't was not possible to delete' });
    }
  },

  async show(req, res) {
    try {

      const user = await User.findByPk(req.entity.id);

      if (!user) {
        return res.status(400).json({ error: 'User not found' });
      }

      return res.json(user);

    } catch (error) {
      return res.status(400).json({ error: 'Couldnt show' });
    }
  },

  async login(req, res) {
    try {

      const user = await User.findOne({ where: { email: req.headers.email } });

      if (!user || user.excluded) {
        return res.status(400).json({ error: 'Email not found' });
      }

      if (!req.headers.password || !await bcrypt.compare(req.headers.password, user.password)) {
        return res.status(400).json({ error: 'Password invalid' });
      }

      const token = jwt.sign({ id: user.id_user }, authConfig.secret, {
        expiresIn: 86400,
      });

      return res.send({ user, token });

    } catch (error) {
      return res.status(400).json({ error: 'Couldnt login' });
    }
  },
}