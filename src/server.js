require('dotenv/config');
require('./database');

const express = require('express');

const routes = require('./routes');
const security = require('./middleware/security');

const app = express();
app.use(security);
app.use(express.json());
app.use(routes);

const PORT = process.env.PORT || 3334;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
